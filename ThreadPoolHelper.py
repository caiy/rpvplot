from multiprocessing import Process,Pool,Manager

def done(the_function, input_parameter_list, n_processes):
    manager = Manager()
    in_factory_pds = manager.list()
    
    count = 0
    _pool = Pool(processes = n_processes)
    for each_pmssm_file in input_parameter_list :
        print(each_pmssm_file)
        _pool.apply_async(the_function, args=(each_pmssm_file, in_factory_pds) )
        count = count+1
        print(count,len(input_parameter_list))
    _pool.close()
    _pool.join()

