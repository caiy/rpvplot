import ROOT
import os
from collections import defaultdict
import math
import json

def loadJson(inpath):
    a={}
    with open(inpath,'r') as infile:
        a = json.load(infile)

def getHisto(inlist, key_name, histo_name, NbinsX, Xmin, Xmax , weight , cut = "" ):
    if cut == "" :
        v = [ obj[key_name] for obj in inlist ]
        w = [ obj[weight] for obj in inlist ]
    else :
        v = [ obj[key_name] for obj in inlist if eval(cut) ]
        w = [ obj[weight] for obj in inlist if eval(cut) ]
    hist = ROOT.TH1F(histo_name, histo_name, NbinsX, Xmin, Xmax)
    hist.Sumw2()
    for i in range(len(v)):
        if len(w) > 0:
            hist.Fill(v[i],w[i])
        else:
            hist.Fill(v[i])
    return hist


def get2DHisto(inlist, key_name_x, key_name_y, histo_name, NbinsX, Xmin, Xmax, NbinsY, Ymin, Ymax, weight , cut = "" ):
    if cut == "" :
        u = [ obj[key_name_x] for obj in inlist ]
        v = [ obj[key_name_y] for obj in inlist ]
        w = [ obj[weight] for obj in inlist ]
    else :
        u = [ obj[key_name_x] for obj in inlist if eval(cut) ]
        v = [ obj[key_name_y] for obj in inlist if eval(cut) ]
        w = [ obj[weight] for obj in inlist if eval(cut) ]
    hist = ROOT.TH2F(histo_name, histo_name, NbinsX, Xmin, Xmax, NbinsY, Ymin, Ymax )
    hist.Sumw2()
    for i in range(len(u)):
        if len(w) > 0:
            hist.Fill(u[i],v[i],w[i])
        else:
            hist.Fill(u[i],u[i])
    return hist



if __name__ == "__main__":
    
    colorPad=[ 920+2, 840-8, 400-6, 860-8, 632-5, 880-8, 9, 6, 2, 1, ]
    
    rangeSetting={
        "Pt": [10, 0, 100],
        "Eta":[13, 0, 2.6], 
        "d0": [24, -6, 6], 
        "z0sintheta": [24, -6, 6],
    }
    
    branchNames=[
        "Weight",
        "Matched_RecTau_Pt",
        "Matched_TruthTau_Vis_Pt",
        "Matched_TruthTau_Vis_Eta",
        "Matched_TruthTau_d0",
        "Matched_TruthTau_z0sintheta",
        "TruthTau_Vis_Pt",
        "TruthTau_Vis_Eta",
        "TruthTau_d0",
        "TruthTau_z0sintheta",
        "Matched_RecTau_ChargedTrk",
        "TruthTau_ChargedPion",
        "TruthTau_NeutralPion",
        "Matched_TruthTau_ChargedPion",
        "Matched_TruthTau_NeutralPion",
    ]
    
    totalpath="/publicfs/atlas/atlasnew/SUSY/users/caiyc/RPV/EfficiencyStudy/xaodlearning/run/"
    rootList={
        "300_0p1ns": 
            [totalpath+"GMSB_300_0_0p1ns_r12307/data-ANALYSIS/mc16_13TeV.399014.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p1ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
             totalpath+"GMSB_300_0_0p1ns_r12184/data-ANALYSIS/mc16_13TeV.399014.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p1ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root"
             ],
        "300_0p01ns": 
            [totalpath+"GMSB_300_0_0p01ns_r12184/data-ANALYSIS/mc16_13TeV.399013.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p01ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_300_0_0p01ns_r12307/data-ANALYSIS/mc16_13TeV.399013.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p01ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
            ],
        "300_0p001ns": 
            [totalpath+"GMSB_300_0_0p001ns_r12184/data-ANALYSIS/mc16_13TeV.399012.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_300_0_0p001ns_r12307/data-ANALYSIS/mc16_13TeV.399012.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root"
            ],
        "300_RPC":
            [totalpath+"RPC_300_1_r10724/data-ANALYSIS/mc16_13TeV.501099.MGPy8EG_StauStauDirect_300p0_1p0_TFilt.deriv.DAOD_SUSY3.e8263_a875_r10724_p4029.root"],
        "200_0p001ns":
            [totalpath+"GMSB_200_0_0p001ns_r12184/data-ANALYSIS/mc16_13TeV.399008.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_200_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_200_0_0p001ns_r12307/data-ANALYSIS/mc16_13TeV.399008.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_200_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
            ],
        "400_0p001ns":
            [totalpath+"GMSB_400_0_0p001ns_r12184/data-ANALYSIS/mc16_13TeV.399016.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_400_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_400_0_0p001ns_r12307/data-ANALYSIS/mc16_13TeV.399016.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_400_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
            ],
    }
    merged_rootList={
        "merged": 
            [totalpath+"GMSB_300_0_0p1ns_r12307/data-ANALYSIS/mc16_13TeV.399014.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p1ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
             totalpath+"GMSB_300_0_0p1ns_r12184/data-ANALYSIS/mc16_13TeV.399014.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p1ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_300_0_0p01ns_r12184/data-ANALYSIS/mc16_13TeV.399013.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p01ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_300_0_0p01ns_r12307/data-ANALYSIS/mc16_13TeV.399013.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p01ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
             totalpath+"GMSB_300_0_0p001ns_r12184/data-ANALYSIS/mc16_13TeV.399012.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_300_0_0p001ns_r12307/data-ANALYSIS/mc16_13TeV.399012.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
             totalpath+"RPC_300_1_r10724/data-ANALYSIS/mc16_13TeV.501099.MGPy8EG_StauStauDirect_300p0_1p0_TFilt.deriv.DAOD_SUSY3.e8263_a875_r10724_p4029.root",
             totalpath+"GMSB_200_0_0p001ns_r12184/data-ANALYSIS/mc16_13TeV.399008.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_200_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_200_0_0p001ns_r12307/data-ANALYSIS/mc16_13TeV.399008.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_200_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
             totalpath+"GMSB_400_0_0p001ns_r12184/data-ANALYSIS/mc16_13TeV.399016.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_400_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_400_0_0p001ns_r12307/data-ANALYSIS/mc16_13TeV.399016.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_400_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
            ],
    }
    
    
    Matched_TruthTaus_all = {}
    TruthTaus_all = {}
    
    for ntuplename,rootfile_list in rootList.items():
        
        Matched_TruthTaus = []
        TruthTaus = []
        for each_root_file in rootfile_list:
            f = ROOT.TFile.Open( each_root_file )
            for event in f.analysis:
                for i in range(len(event.Matched_TruthTau_Vis_Pt)):
                    Matched_TruthTaus.append({
                        "weight"      : event.Weight,
                        "Pt"          : event.Matched_TruthTau_Vis_Pt[i]/1000,
                        "Eta"         : abs(event.Matched_TruthTau_Vis_Eta[i]),
                        "d0"          : math.log(abs(event.Matched_TruthTau_d0[i])),
                        "z0sintheta"  : math.log(abs(event.Matched_TruthTau_z0sintheta[i])),
                        "ChargedPion" : event.Matched_TruthTau_ChargedPion[i],
                        "NeutralPion" : event.Matched_TruthTau_NeutralPion[i],
                    })
                for i in range(len(event.TruthTau_Vis_Pt)):
                    TruthTaus.append({
                        "weight"      : event.Weight,
                        "Pt"          : event.TruthTau_Vis_Pt[i]/1000,
                        "Eta"         : abs(event.TruthTau_Vis_Eta[i]),
                        "d0"          : math.log(abs(event.TruthTau_d0[i])),
                        "z0sintheta"  : math.log(abs(event.TruthTau_z0sintheta[i])),
                        "ChargedPion" : event.TruthTau_ChargedPion[i],
                        "NeutralPion" : event.TruthTau_NeutralPion[i],
                    })       
       
        Matched_TruthTaus_all[ntuplename] = Matched_TruthTaus
        TruthTaus_all[ntuplename] = TruthTaus



    """
    drawing 1D distribution,  compare different samples
    """
    #for prongcut in [ 'obj["ChargedPion"]>=0', 'obj["ChargedPion"]==1', 'obj["ChargedPion"]==3' ] :
    #    for var in ["Pt", "Eta", "d0", "z0sintheta"] :
    #
    #        hist_list = []
    #        hist_name = []
    #        for ntuplename,rootfile_list in Matched_TruthTaus_all.items():
    #            #hist_n = getHisto(TruthTaus_all[ntuplename],         var, "AllTruth_Eta_d_all", rangeSetting[var][0], rangeSetting[var][1], rangeSetting[var][2], "weight", prongcut)
    #            hist_d = getHisto(Matched_TruthTaus_all[ntuplename], var, "AllTruth_Eta_n_all", rangeSetting[var][0], rangeSetting[var][1], rangeSetting[var][2], "weight", prongcut)
    #            #hist_d.Divide(hist_n)
    #            hist_d.Scale(1/hist_d.Integral())
    #            hist_list.append( hist_d )
    #            hist_name.append( ntuplename)
    #
    #        canvas = ROOT.TCanvas("canvas", "Canvas Title", 1600, 1200)
    #        hist_list[0].Draw('PL')
    #        hist_list[0].SetLineWidth(5)
    #        hist_list[0].SetLineColor(colorPad[0])
    #        for i in range(1, len(hist_list)):
    #            hist_list[i].Draw('SAME PL')
    #            hist_list[i].SetLineWidth(5)
    #            hist_list[i].SetLineColor(colorPad[i])
    #        hist_list[0].SetXTitle(var)
    #        hist_list[0].SetYTitle("fraction")
    #        hist_list[0].GetYaxis().SetRangeUser(0, 0.6)
    #        hist_list[0].SetStats(0)
    #        hist_list[0].SetTitle("distribution_prong"+prongcut[-1]+"_"+var)
    #        legend = ROOT.TLegend(0.1,0.7,0.3,0.9)
    #        for i in range(len(hist_list)):
    #            legend.AddEntry(hist_list[i], hist_name[i],"lep")
    #        legend.Draw()
    #        canvas.SaveAs("distribution1_All"+prongcut[-1]+"_"+var+".png")



    """
    drawing 1D efficiency , compare different samples
    """
    #for prongcut in [ 'obj["ChargedPion"]>=0', 'obj["ChargedPion"]==1', 'obj["ChargedPion"]==3' ] :
    #    for var in ["Pt", "Eta", "d0", "z0sintheta"] :
    #
    #        hist_list = []
    #        hist_name = []
    #        for ntuplename,rootfile_list in Matched_TruthTaus_all.items():
    #            hist_n = getHisto(TruthTaus_all[ntuplename],         var, "AllTruth_Eta_d_all", rangeSetting[var][0], rangeSetting[var][1], rangeSetting[var][2], "weight", prongcut)
    #            hist_d = getHisto(Matched_TruthTaus_all[ntuplename], var, "AllTruth_Eta_n_all", rangeSetting[var][0], rangeSetting[var][1], rangeSetting[var][2], "weight", prongcut)
    #            hist_d.Divide(hist_n)
    #            hist_list.append( hist_d )
    #            hist_name.append( ntuplename)
    #
    #        canvas = ROOT.TCanvas("canvas", "Canvas Title", 1600, 1200)
    #        hist_list[0].Draw('PL')
    #        hist_list[0].SetLineWidth(5)
    #        hist_list[0].SetLineColor(colorPad[0])
    #        for i in range(1, len(hist_list)):
    #            hist_list[i].Draw('SAME PL')
    #            hist_list[i].SetLineWidth(5)
    #            hist_list[i].SetLineColor(colorPad[i])
    #        hist_list[0].SetXTitle(var)
    #        hist_list[0].SetYTitle("efficiency")
    #        hist_list[0].GetYaxis().SetRangeUser(0, 1.4)
    #        hist_list[0].SetStats(0)
    #        hist_list[0].SetTitle("efficiency_prong"+prongcut[-1]+"_"+var)
    #        legend = ROOT.TLegend(0.1,0.7,0.3,0.9)
    #        for i in range(len(hist_list)):
    #            legend.AddEntry(hist_list[i], hist_name[i],"lep")
    #        legend.Draw()
    #        canvas.SaveAs("eff1_All"+prongcut[-1]+"_"+var+".png")



    """
    drawing 1D efficiency , split different samples
    """
    #for var in ["Pt", "Eta", "d0", "z0sintheta"]:
    #
    #    hist_down_all = getHisto(TruthTaus_all[ntuplename],         var, "AllTruth_Eta_d_all", rangeSetting[var][0], rangeSetting[var][1], rangeSetting[var][2], "weight")
    #    hist_all      = getHisto(Matched_TruthTaus_all[ntuplename], var, "AllTruth_Eta_n_all", rangeSetting[var][0], rangeSetting[var][1], rangeSetting[var][2], "weight")
    #    hist_all.Divide(hist_down_all)
    #
    #    hist_down_1p = getHisto(TruthTaus_all[ntuplename],    var, "AllTruth_Eta_d_1p", rangeSetting[var][0], rangeSetting[var][1], rangeSetting[var][2], "weight" , 'obj["ChargedPion"]==1' )
    #    hist_1p = getHisto(Matched_TruthTaus_all[ntuplename], var, "AllTruth_Eta_n_1p", rangeSetting[var][0], rangeSetting[var][1], rangeSetting[var][2], "weight" , 'obj["ChargedPion"]==1' )
    #    hist_1p.Divide(hist_down_1p)
    #
    #    hist_down_3p = getHisto(TruthTaus_all[ntuplename],    var, "AllTruth_Eta_d_3p", rangeSetting[var][0], rangeSetting[var][1], rangeSetting[var][2], "weight" , 'obj["ChargedPion"]==3' )
    #    hist_3p = getHisto(Matched_TruthTaus_all[ntuplename], var, "AllTruth_Eta_n_3p", rangeSetting[var][0], rangeSetting[var][1], rangeSetting[var][2], "weight" , 'obj["ChargedPion"]==3' )
    #    hist_3p.Divide(hist_down_3p)
    #    
    #    canvas = ROOT.TCanvas("canvas", "Canvas Title", 1600, 1200)
    #
    #    hist_all.Draw('PLC PMC')
    #    hist_1p.Draw('SAME PLC PMC')
    #    hist_3p.Draw('SAME PLC PMC')
    #    hist_all.SetLineWidth(6)
    #    hist_1p.SetLineWidth(6)
    #    hist_3p.SetLineWidth(6)
    #    
    #    hist_all.SetXTitle(var)
    #    hist_all.SetYTitle("efficiency")
    #    hist_all.GetYaxis().SetRangeUser(0, 1.4)
    #    
    #    hist_all.SetStats(0)
    #
    #    hist_all.SetTitle("efficiency_"+var)
    #    
    #    legend = ROOT.TLegend(0.1,0.7,0.3,0.9)
    #    legend.AddEntry(hist_all,"All Had Tau","lep")
    #    legend.AddEntry(hist_1p, "Truth 1-prong","lep")
    #    legend.AddEntry(hist_3p, "Truth 3-prong","lep")
    #    legend.Draw()
    #    
    #    canvas.SaveAs("eff1_"+var+"_"+ntuplename+".png")
    #                