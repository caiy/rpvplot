import uproot
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import pickle



def gaus(x,a,x0,sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))


def loadROOT( filepath , treename, branches=[]):
    with uproot.open(filepath) as file:
        print(file.keys())
        tree = file[treename]
        tree.keys()
        print(tree.keys())
        return tree.arrays(branches)
        
        #events = tree.arrays()
        #print(events[:2].tolist())

def To1DHisto( inputlist, weight, bins, xrange=[0,1], density=False ):
    counts, bins_edge = np.histogram( inputlist, bins=bins, range=xrange, weights=weight, density=density )
    bin_centers=[]
    for iii in range(len(bins_edge)-1) :
        bin_centers.append( (bins_edge[iii] + bins_edge[iii+1])/2 )
    
    weight_ = np.array(weight)
    n_err = np.sqrt(np.histogram(inputlist, bins=bins, range=xrange, weights=weight_**2)[0])
    
    return counts, n_err, bin_centers, bins_edge

def dumpToPickle(in_dict, outname):
    with open(outname+'.pickle', 'w') as handle:
        pickle.dump(in_dict, handle, protocol=0)

if __name__ == "__main__":
    
    branchNames=[
        "Weight",
        "Matched_RecTau_Pt",
        "Matched_TruthTau_Vis_Pt",
        "Matched_TruthTau_Vis_Eta",
        "Matched_TruthTau_d0",
        "Matched_TruthTau_z0sintheta",
        "TruthTau_Vis_Pt",
        "TruthTau_Vis_Eta",
        "TruthTau_d0",
        "TruthTau_z0sintheta",
        
        "Matched_RecTau_ChargedTrk",
        "TruthTau_ChargedPion",
        "TruthTau_NeutralPion",
        "Matched_TruthTau_ChargedPion",
        "Matched_TruthTau_NeutralPion",
                 ]
    
    totalpath="/publicfs/atlas/atlasnew/SUSY/users/caiyc/RPV/EfficiencyStudy/xaodlearning/run/"
    rootList={
        "300_0p1ns": 
            [totalpath+"GMSB_300_0_0p1ns_r12307/data-ANALYSIS/mc16_13TeV.399014.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p1ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
             totalpath+"GMSB_300_0_0p1ns_r12184/data-ANALYSIS/mc16_13TeV.399014.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p1ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root"
             ],
        "300_0p01ns": 
            [totalpath+"GMSB_300_0_0p01ns_r12184/data-ANALYSIS/mc16_13TeV.399013.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p01ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_300_0_0p01ns_r12307/data-ANALYSIS/mc16_13TeV.399013.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p01ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
            ],
        "300_0p001ns": 
            [totalpath+"GMSB_300_0_0p001ns_r12184/data-ANALYSIS/mc16_13TeV.399012.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_300_0_0p001ns_r12307/data-ANALYSIS/mc16_13TeV.399012.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root"
            ],
        "300_RPC":
            [totalpath+"RPC_300_1_r10724/data-ANALYSIS/mc16_13TeV.501099.MGPy8EG_StauStauDirect_300p0_1p0_TFilt.deriv.DAOD_SUSY3.e8263_a875_r10724_p4029.root"],
        "200_0p001ns":
            [totalpath+"GMSB_200_0_0p001ns_r12184/data-ANALYSIS/mc16_13TeV.399008.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_200_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_200_0_0p001ns_r12307/data-ANALYSIS/mc16_13TeV.399008.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_200_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
            ],
        "400_0p001ns":
            [totalpath+"GMSB_400_0_0p001ns_r12184/data-ANALYSIS/mc16_13TeV.399016.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_400_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_400_0_0p001ns_r12307/data-ANALYSIS/mc16_13TeV.399016.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_400_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
            ],
    }
    
    merged_rootList={
        "merged": 
            [totalpath+"GMSB_300_0_0p1ns_r12307/data-ANALYSIS/mc16_13TeV.399014.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p1ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
             totalpath+"GMSB_300_0_0p1ns_r12184/data-ANALYSIS/mc16_13TeV.399014.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p1ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_300_0_0p01ns_r12184/data-ANALYSIS/mc16_13TeV.399013.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p01ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_300_0_0p01ns_r12307/data-ANALYSIS/mc16_13TeV.399013.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p01ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
             totalpath+"GMSB_300_0_0p001ns_r12184/data-ANALYSIS/mc16_13TeV.399012.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_300_0_0p001ns_r12307/data-ANALYSIS/mc16_13TeV.399012.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
             totalpath+"RPC_300_1_r10724/data-ANALYSIS/mc16_13TeV.501099.MGPy8EG_StauStauDirect_300p0_1p0_TFilt.deriv.DAOD_SUSY3.e8263_a875_r10724_p4029.root",
             totalpath+"GMSB_200_0_0p001ns_r12184/data-ANALYSIS/mc16_13TeV.399008.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_200_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_200_0_0p001ns_r12307/data-ANALYSIS/mc16_13TeV.399008.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_200_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
             totalpath+"GMSB_400_0_0p001ns_r12184/data-ANALYSIS/mc16_13TeV.399016.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_400_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12184_p5386.root",
             totalpath+"GMSB_400_0_0p001ns_r12307/data-ANALYSIS/mc16_13TeV.399016.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_400_0_0p001ns.deriv.DAOD_SUSY3.e6633_s3307_r12307_p5386.root",
            ],
    }


        
    
    # Draw resolution
    inputs_tmp = []
    for ntuplename,rootfile_list in rootList.items():
        #inputs_tmp.append([ntuplename,rootfile_list])
        #done(PlotPtResolution,inputs_tmp,5)
        
        weight = []
        truthMatched_recoPt = []
        truthMatched_truthVisPt = []
        truthMatched_recoPt_over_truthVis = []
        
        for rootfile in rootfile_list:
            events = loadROOT(rootfile,"analysis",branchNames)
            #print(events[:2].tolist())
            #print(type(events))
            for event in events:
                for i in range(len(event["Matched_RecTau_Pt"])):
                    truthMatched_recoPt.append(event["Matched_RecTau_Pt"][i])
                    truthMatched_truthVisPt.append(event["Matched_TruthTau_Vis_Pt"][i])
                    truthMatched_recoPt_over_truthVis.append( event["Matched_RecTau_Pt"][i]/event["Matched_TruthTau_Vis_Pt"][i] )
                    weight.append(event["Weight"])
            
        counts, counts_err, bin_centers, bins_edge = To1DHisto( truthMatched_recoPt_over_truthVis, bins=100, xrange=[0 , 2  ], weight=weight, density=False  )
        #counts_fit, counts_err_fit, bin_centers_fit, bins_edge_fit = To1DHisto( truthMatched_recoPt_over_truthVis, bins=20, xrange=[0.8,1.2], weight=weight, density=False  )
        dump_dict={
            "yield": counts, 
            "error": counts_err, 
            "bincenter": bin_centers, 
            "bin_edge": bins_edge,
        }
        dumpToPickle(dump_dict , ntuplename)
    

        
        #popt,pcov = curve_fit(gaus,bin_centers_fit,counts_fit,p0=[max(counts_fit),1,0.08], sigma=counts_err_fit )
        #gaussian = gaus(bins_edge,*popt)
        #print(*popt)
        #plt.plot(bins_edge, gaussian, c='blue')
        #plt.plot(bin_centers, counts, 'o', color='black')
        #plt.annotate("x_0 = " + str(round(popt[1],3))                  , xy=(0.05, 0.9), xycoords='axes fraction')
        #plt.annotate("FWHM = " + str(np.abs( round( 2.355*popt[2] ,3))), xy=(0.05, 0.8), xycoords='axes fraction')
        #plt.annotate(ntuplename                                        , xy=(0.05, 0.7), xycoords='axes fraction')
        #plt.savefig(ntuplename+'.png')
        #plt.clf()
    
    ## Draw Efficiency
    #for ntuplename,rootfile_list in merged_rootList.items():
    #    
    #    weight_matched = []
    #    weight_total = []
    #    
    #    list_Matched_TruthTau_Vis_Pt      = [] # "Matched_TruthTau_Vis_Pt     "
    #    list_Matched_TruthTau_Vis_Eta     = [] # "Matched_TruthTau_Vis_Eta    "
    #    list_Matched_TruthTau_d0          = [] # "Matched_TruthTau_d0         "
    #    list_Matched_TruthTau_z0sintheta  = [] # "Matched_TruthTau_z0sintheta "
    #    list_Matched_TruthTau_ChargedPion = [] # "Matched_TruthTau_ChargedPion"
    #    list_Matched_TruthTau_NeutralPion = [] # "Matched_TruthTau_NeutralPion"
    #    list_Matched_RecTau_ChargedTrk    = [] # "Matched_RecTau_ChargedTrk   "
    #    
    #    list_TruthTau_Vis_Pt      = [] # "TruthTau_Vis_Pt"     
    #    list_TruthTau_Vis_Eta     = [] # "TruthTau_Vis_Eta"    
    #    list_TruthTau_d0          = [] # "TruthTau_d0"         
    #    list_TruthTau_z0sintheta  = [] # "TruthTau_z0sintheta" 
    #    list_TruthTau_ChargedPion = [] # "TruthTau_ChargedPion"
    #    list_TruthTau_NeutralPion = [] # "TruthTau_NeutralPion"
    #
    #    
    #    for rootfile in rootfile_list:
    #        events = loadROOT(rootfile,"analysis",branchNames)
    #        #print(events[:2].tolist())
    #        #print(type(events))
    #        for event in events:
    #            for i in range(len(event["Matched_RecTau_Pt"])):
    #                weight_matched.append(event["Weight"])
    #                list_Matched_TruthTau_Vis_Pt     .append(   event["Matched_TruthTau_Vis_Pt"     ][i] )
    #                list_Matched_TruthTau_Vis_Eta    .append(   event["Matched_TruthTau_Vis_Eta"    ][i] )
    #                list_Matched_TruthTau_d0         .append(   np.log10(event["Matched_TruthTau_d0"         ][i]) )
    #                list_Matched_TruthTau_z0sintheta .append(   np.log10(event["Matched_TruthTau_z0sintheta" ][i]) )
    #                list_Matched_TruthTau_ChargedPion.append(   event["Matched_TruthTau_ChargedPion"][i] )
    #                list_Matched_TruthTau_NeutralPion.append(   event["Matched_TruthTau_NeutralPion"][i] )
    #                list_Matched_RecTau_ChargedTrk   .append(   event["Matched_RecTau_ChargedTrk"   ][i] )
    #            for i in range(len(event["TruthTau_Vis_Pt"])):
    #                weight_total.append(event["Weight"])
    #                list_TruthTau_Vis_Pt     .append( event["TruthTau_Vis_Pt"     ][i] )
    #                list_TruthTau_Vis_Eta    .append( event["TruthTau_Vis_Eta"    ][i] )
    #                list_TruthTau_d0         .append( np.log10(event["TruthTau_d0"         ][i]) )
    #                list_TruthTau_z0sintheta .append( np.log10(event["TruthTau_z0sintheta" ][i]) )
    #                list_TruthTau_ChargedPion.append( event["TruthTau_ChargedPion"][i] )
    #                list_TruthTau_NeutralPion.append( event["TruthTau_NeutralPion"][i] )

#    def PlotPtResolution(inputs):
#        print("?????????")
#        ntuplename = inputs[0]
#        rootfile_list = inputs[1]
#        weight = []
#        truthMatched_recoPt = []
#        truthMatched_truthVisPt = []
#        truthMatched_recoPt_over_truthVis = []
#        
#        for rootfile in rootfile_list:
#            events = loadROOT(rootfile,"analysis",branchNames)
#            #print(events[:2].tolist())
#            #print(type(events))
#            for event in events:
#                for i in range(len(event["Matched_RecTau_Pt"])):
#                    truthMatched_recoPt.append(event["Matched_RecTau_Pt"][i])
#                    truthMatched_truthVisPt.append(event["Matched_TruthTau_Vis_Pt"][i])
#                    truthMatched_recoPt_over_truthVis.append( event["Matched_RecTau_Pt"][i]/event["Matched_TruthTau_Vis_Pt"][i] )
#                    weight.append(event["Weight"])
#            
#        counts, counts_err, bin_centers, bins_edge = To1DHisto( truthMatched_recoPt_over_truthVis, bins=100, xrange=[0,2], weight=weight, density=False  )
#
#        popt,pcov = curve_fit(gaus,bin_centers,counts,p0=[1,1,0.5], sigma=counts_err )
#        gaussian = gaus(bins_edge,*popt)
#        print(*popt)
#        plt.plot(bins_edge, gaussian, c='blue')
#        plt.plot(bin_centers, counts, 'o', color='black')
#        plt.annotate("x_0 = " + str(round(popt[1],3))                  , xy=(0.05, 0.9), xycoords='axes fraction')
#        plt.annotate("FWHM = " + str(np.abs( round( 2.355*popt[2] ,3))), xy=(0.05, 0.8), xycoords='axes fraction')
#        plt.annotate(ntuplename                                        , xy=(0.05, 0.7), xycoords='axes fraction')
#        plt.savefig(ntuplename+'.png')
#        plt.clf()